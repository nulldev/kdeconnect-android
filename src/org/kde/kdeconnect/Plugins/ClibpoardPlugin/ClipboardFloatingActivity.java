/*
 * SPDX-FileCopyrightText: 2020 Anjani Kumar <anjanik012@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

package org.kde.kdeconnect.Plugins.ClibpoardPlugin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import org.kde.kdeconnect_tp.R;

/*
    An activity to access the clipboard on Android 10 and later by raising over other apps.
    This is invisible and doesn't require any interaction from the user.
    This should be called when a change in clipboard is detected. This can be done by manually
    when user wants to send the clipboard or by reading system log files which requires a special
    privileged permission android.permission.READ_LOGS.
    https://developer.android.com/reference/android/Manifest.permission#READ_LOGS
    This permission can be gained by only from the adb by the user.
    https://www.reddit.com/r/AndroidBusters/comments/fh60lt/how_to_solve_a_problem_with_the_clipboard_on/

    Currently this activity is bering triggered from a button in Foreground Notification.
* */
public class ClipboardFloatingActivity extends AppCompatActivity {

    public static Intent getIntent(Context context) {
        Intent startIntent = new Intent(context.getApplicationContext(), ClipboardFloatingActivity.class);
        startIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        return startIntent;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            // We are now sure that clipboard can be accessed from here.
            ClipboardListener.instance(this).onClipboardChanged();
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clipboard_floating);
        WindowManager.LayoutParams wlp = getWindow().getAttributes();
        wlp.dimAmount = 0;
        wlp.flags = WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS |
                WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL;

        getWindow().setAttributes(wlp);
    }
}

